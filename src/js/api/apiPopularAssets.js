let data;
let page = 1;
let moreTokens = document.getElementById('more');
let endList = document.querySelector('#end');
moreTokens.style.color.hover = ("white");

moreTokens.addEventListener("click", () => {

    if (page === 3) {
        moreTokens.style.display = "none";
        endList.style.display = 'block';




    } else {
        page += 1;
        obtenerTokensPopulares()
    }
})

let popularTokenList = (data) => {
    data = data;
    let loading = document.getElementById('loading').textContent = "";
    endList.style.display = 'none';

    data.forEach(token => {
        //declaration
        let listBase = document.getElementById("list-tokens");
        let li = document.createElement('li');
        let p = document.createElement('p');
        let img = document.createElement('img')
            //set
        li.className = `token-element`;
        p.innerHTML = `<b>${token.id}</b><br> price: ${Math.abs(token.price).toPrecision(8)} € <br> Rank #${token.rank}`;
        img.setAttribute("src", token.logo_url);
        img.style.width = "5%";
        img.style.height = "auto";
        img.style.padding = "30px 30px ";
        img.id = `${token.id}`;
        //put
        li.style.animation = "fadeIn 2s";
        listBase.appendChild(li);
        li.appendChild(img);
        li.appendChild(p);

    })
};


let obtenerTokensPopulares = () => {

    fetch(`https://api.nomics.com/v1/currencies/ticker?key=74d547bd8c383b700a0dbdcf7e31194e2ee5604b&ids&interval=1d&convert=EUR&per-page=15&page=${page}`)
        .then(jsonData => jsonData.json())
        .then(data => popularTokenList(data))
}


obtenerTokensPopulares();