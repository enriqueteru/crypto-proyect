const urls = ["https://reqres.in/api/users?page=1",
    "https://reqres.in/api/users?page=2"
];

let listOfUsers = [];

const getData = () => {

    Promise.all(urls.map(url => fetch(url)
        .then(resp => resp.json())
        .then(data => {

            data = data.data;
            for (let index = 0; index <= 5; index++) {
                const element = data[index];
                listOfUsers.push(element.first_name);

            }
        })

    ))


}


getData();

export { listOfUsers }