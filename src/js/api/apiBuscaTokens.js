let tokenSearched = document.getElementById("tokenSearch").value;
let bntSearcher = document.getElementById("btnSearcher");
let bntRestart = document.getElementById("restartSearcher");
let divResult = document.getElementById("divResult");
let divSearchResult = document.getElementById("divSearchResult");
bntRestart.style.display = "none";
let arrayTokens = [];

let searching = () => {
    bntSearcher.addEventListener("click", () => {
        const key = "74d547bd8c383b700a0dbdcf7e31194e2ee5604b&ids&ids";
        const url = `https://api.nomics.com/v1/currencies/ticker?key=${key}&interval=1d&convert=EUR&per-page=500&page=1`;

        fetch(url)
            .then((response) => response.json())
            .then((data) => {
                let inputvalue = document.getElementById("tokenSearch").value;
                for (let index = 0; index < data.length; index++) {
                    const element = data[index];
                    nameT = element.name;
                    arrayTokens.push(nameT)




                    if (
                        element.name == inputvalue ||
                        element.name.toLowerCase() == inputvalue ||
                        element.name.toUpperCase() == inputvalue || element.id.toUpperCase() == inputvalue || element.id.toLowerCase() == inputvalue
                    ) {
                        let div = document.createElement("div");
                        div.innerHTML = `<div class="card-result">
                        <img src="${element.logo_url}" width = "10%" height ="auto">
                        Rank: #${element.rank}<br>
                        Name: ${element.name}<br>
                        Price: ${Math.abs(element.price).toPrecision(8)}<br>
                        Actual supply: ${element.circulating_supply}<br>
                        Final supply: ${element.max_supply}<br>
                        Status: ${element.status}</div>`;
                        divResult.appendChild(div);
                        div.setAttribute("id", "divSearchResult");
                        bntSearcher.style.display = "none";
                        bntRestart.style.display = "block";
                    } else {}
                }
            });
    });
};

let clean = () => {
    bntRestart.addEventListener("click", () => {
        bntSearcher.style.display = "block";
        bntRestart.style.display = "none";
        document.getElementById("tokenSearch").value = ``;
        document.getElementById("divSearchResult").remove();
    });
};








searching();
clean();