window.onscroll = () => {

    let upButton = document.getElementById('up');

    if (document.body.scrollTop > 800 || document.documentElement.scrollTop > 800) {
        upButton.style.display = "block";
        upButton.style.animation = "fadeIn 2s";

    } else {
        upButton.style.display = "none";
    }

};