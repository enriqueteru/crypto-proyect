const modal = document.getElementById("cryptoModal");
const formModal = document.getElementById("loginForm");
const modalBtnLogin = document.querySelector("#special")

//open modal event
modalBtnLogin.addEventListener("click", ()=> {
    modal.style.display = "block";

})
//close modal event clicking in "X"
const closeModal = document.querySelector(".modal__close");
closeModal.addEventListener("click", ()=> {
modal.style.display = "none";

})

 //close modal event clicking outside
window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none"; 
    }
  }


  export {modal};
  export {formModal};
  export {modalBtnLogin};