// Scripts

import "./js/script.js";

//components
import "./js/scripts/balance-acount.js";
import "./js/scripts/cambia-token.js";
import "./js/scripts/modal.js";
import "./js/scripts/login.js";

//Elements
import './js/scripts/upButton'

//Api rest Fetch
import './js/api/apiPopularAssets.js';
import './js/api/apiTableRank.js';
import "./js/api/apiBuscaTokens.js";
import "./js/api/infiniteScroll.js";
import './js/scripts/print.js';
import './js/api/setIntervalnames.js';
import './js/api/walletdata.js';
import './js/api/conversor.js';

// Styles
import "./scss/main.scss";

//effects
import "./js/effects/scrollReveal";